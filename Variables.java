package scripts.clayMaster_Red;

import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;
 
/*This class will be used to store most of the global variable, it
 * will also have methods to change them and return there values
 */
public class Variables {
 
        private boolean run = true;//Used to break out of the mainLoop
        private boolean paint = true;
        
        
        //GUI Setting
        private int mouseSpeed = 100;
        private int defMouseSpeed = mouseSpeed;
        private int location;//0 = Edgeville //TODO Still unused
        private boolean fillBuckets;
        private boolean makeSoftClay;
        private boolean tidyBank;
        private boolean isGUIOpen = true;
        private boolean simplePaint = true;
        private boolean useGE=false;
        //End of GUI Settings
        
        //Final variables
        private static final int[] BUCKET_IDS = { 1925, 1929 };// 0-Empty, 1-Water.
        private static final int BANK_BOOTH_ID = 11744,// Edgeville bank booth id.
                                                        WELL_ID = 884,// Edgeviller well id.
                                                        CLAY_ID = 434,
                                                        SOFT_CLAY_ID = 1761;
  
        //RS Areas
        private static final RSArea[] BANK_AREA = {(new RSArea(new RSTile(3098, 3494, 0), new RSTile(3092, 3498, 0))),(new RSArea(new RSTile(3094, 3493, 0), new RSTile(3092, 3488, 0)))};
        private static final RSArea WELL_AREA = new RSArea(new RSTile(3086, 3500, 0), new RSTile(3083, 3505, 0));
        
        
        public static final boolean DEBUG = false;
        // End of Final variables
       
        //Start of GLOBAL VARIABLES
        private int stage;//stage 0:Bucket Filling
                                                                //stage 1:Making Soft Clay
                                                                //stage 2:Using GE(in the future)
        private boolean needRestock = true;//If true will start banking
        private long clayMade = 0;
        private String action = "None";
       
        //End of GLOBAL VARIABLES
       
        
        //Methods
        public void setPaint(boolean b){
                paint = b;
        }
        public boolean paint(){
                return paint;
        }
       
        public boolean run(){
                return run;
        }
        public void setRun(boolean b){
                run = b;
        }
       
        public int getMouseSpeed(){
                return mouseSpeed;
        }
        public void setMouseSpeed(int mouseSpeed){
                this.mouseSpeed = mouseSpeed;
        }
        public void increaseMouseSpeedBy(int speed){
                mouseSpeed+=speed;
        }
        public void decreaseMouseSpeedBy(int speed){
                mouseSpeed-=speed;
        }
        public void resetMouseSpeed(){
                mouseSpeed =defMouseSpeed;
        }
       
        public int location(){
                return location;
        }
        public void location(int i){
                location = i;
        }
       
        public void setFillBuckets(boolean b){
                fillBuckets = b;
        }
        public boolean fillBuckets(){
                return fillBuckets;    
        }
       
        public void setMakeSoftClay(boolean b){
                makeSoftClay = b;
        }
        public boolean makeSoftClay(){
                return makeSoftClay;   
        }
       
        public void setTidyBank(boolean b){
                tidyBank = b;  
        }
        public boolean tidyBank(){
                return tidyBank;
        }
        public int tidyBank(boolean tidyBank){
                if(tidyBank){
                        return 1;
                }else{
                        return 0;
                }
        }
       
       
        public void setStage(int stage){
                if(fillBuckets == false && stage == 0){
                        run = false;
                }else{
                this.stage = stage;
                }
        }
        public int getStage(){
                return stage;
        }
       
        public static int getBucketId(String s){
                String type = s;
                type.trim();
                type.toUpperCase();
                if(type.startsWith("E")){
                        return BUCKET_IDS[0];
                }
                if(type.startsWith("W")){
                        return BUCKET_IDS[1];
                }
                return -1;
        }
        public static int getBankId(){//This will be taking in location as a variable when
                //the script has multiple location support
                return BANK_BOOTH_ID;
        }
        public static int getWaterSource(){//This will be taking in location as a variable
                //when the script has multiple location support
                return WELL_ID;
        }
        public static int getClayId(){
                return CLAY_ID;
        }
        public static RSArea[] getBankArea(){//This will be taking in location as a variable
                //when the script has multiple location support
                return BANK_AREA;
        }

        public static RSArea getWaterSourceArea(){//This will be taking in location as a variable
                //when the script has multiple location support
                return WELL_AREA;
        }
       
        public void setGUIState(boolean b){
                isGUIOpen = b;
        }
        public boolean isGUIOpen(){
                return isGUIOpen;
        }
       
        public void setRestock(boolean b){
                needRestock = b;
        }
        public  boolean needRestock(){
                return needRestock;
        }
        public long clayMade(){
                return clayMade;
        }
        public void addClay(){
                clayMade++;
        }
        public void addClay(int clayMade){
                this.clayMade+=clayMade;
        }
        public String getAction(){
                return action;
        }
        public  void setAction(String action){
                this.action = action;
        }
        public int getSoftClayId(){
                return SOFT_CLAY_ID;
        }
        public boolean simplePaint(){
        	return simplePaint;
        }
		public void setUseGE(boolean selected) {
			this.useGE = selected;
			
		}
		public boolean useGE(){
			return this.useGE();
		}
}
