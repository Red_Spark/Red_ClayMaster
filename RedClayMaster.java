
package scripts.clayMaster_Red;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collections;


import org.tribot.api.General;
import org.tribot.api.input.Mouse;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Ending;
import org.tribot.script.interfaces.Painting;


import java.awt.Color; //to get different colors
import java.awt.Font; //to change font
 
import org.tribot.api.Timing; //to calculate time things
import org.tribot.api2007.Login;

import scripts.redSpark.api.GE07.GE_Check_Official;
 
@ScriptManifest(authors = "Red_Spark", category = "Test", name = "ClayMaster_Red", version = 1.5)
public class RedClayMaster extends Script implements Painting, Ending {
		final ScriptManifest MANIFEST = (ScriptManifest)this.getClass().getAnnotation(ScriptManifest.class);
        public static ArrayList<Node> nodes = new ArrayList<>();
        private Variables variables = new Variables();
        //MANIFEST.authors() not working <_<
        public final String TITLE = MANIFEST.name()+" "+MANIFEST.version()+" by Red_Spark";
        long timeRan;

        public void onStart(){
                General.println(TITLE);
        }     
        public void run() {
                General.println("Script has started");
                GUI gui = new GUI(variables);
                gui.setVisible(true);
                while(variables.isGUIOpen()){//Waiting for the user to input all GUI settings
                        sleep(500);
                }
                // add all nodes to the ArrayList.
                Collections.addAll(nodes, new Banker(variables), new BucketFiller(variables), new SoftClayMaker(variables));
                Mouse.setSpeed(variables.getMouseSpeed());
                General.println(Mouse.getSpeed());
                mainLoop(20, 40);// Takes in (min, max) sleep times to reduce CPU usage.
        }
 
        private void mainLoop(int min, int max) {
                while (variables.run()) {
                        for (final Node node : nodes) { // Loop through the nodes.
                                if (node.validate()) { // Validate each node
                                        node.execute(); // Execute nodes.
                                }else{
                                        sleep(General.random(min, max)); // time in between executing
                                }
                        }
                }
                onStop();     
        }
        public void onStop(){
        	println("Thank you for using:"+MANIFEST.name()+" "+MANIFEST.version());
        	println("You have made "+variables.clayMade()+" soft clay in "+Timing.msToString(timeRan));
        	println("Profit this sesion:"+(SOFT_CLAY_PRICE-CLAY_PRICE)*variables.clayMade());
        	while(!Login.logout()){
        		General.sleep(1000, 3000);
        	}
        	
        }

        public static final long startTime = System.currentTimeMillis();
        final int SOFT_CLAY_PRICE = GE_Check_Official.getPrice(variables.getSoftClayId());
        final int CLAY_PRICE = GE_Check_Official.getPrice(Variables.getClayId());
        
      
        
        public void onPaint(Graphics g) {
        		//if(variables.simplePaint()){
		                timeRan = System.currentTimeMillis() - startTime;
		                g.setFont(new Font("Verdana", Font.BOLD, 20));
		                g.setColor(new Color(255, 0, 51));
		                g.drawString(TITLE, 10, 40);
		               
		                g.setFont(new Font("Verdana", Font.BOLD, 14));
		                g.drawString("Time Running:"+ Timing.msToString(timeRan), 10, 60);
		               
		                g.drawString("Clay Made:"+variables.clayMade(), 10, 80);
		               
		                g.drawString("Clay Made P/H:"+(long)(variables.clayMade() * 3600000 / timeRan), 10, 100);
		               
		                g.drawString("Profit P/H:"+(long)(variables.clayMade() * 3600000 / timeRan)*(SOFT_CLAY_PRICE-CLAY_PRICE), 10, 120);
		               
		                g.drawString("Total Profit:"+(SOFT_CLAY_PRICE-CLAY_PRICE)*variables.clayMade(), 10, 140);
		               
		                g.drawString("Current Action:"+variables.getAction(), 10, 160);
 
      //  }
        }
		@Override
		public void onEnd() {
			println("Thank you for using:"+MANIFEST.name()+" "+MANIFEST.version());
        	println("You have made "+variables.clayMade()+" soft clay in "+Timing.msToString(timeRan));
        	println("Profit this sesion:"+(SOFT_CLAY_PRICE-CLAY_PRICE)*variables.clayMade());
			
		}
       
}
