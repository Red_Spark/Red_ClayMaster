package scripts.clayMaster_Red;

public abstract class Node {

	public abstract void execute();

	public abstract boolean validate();
	
}
