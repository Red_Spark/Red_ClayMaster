package scripts.clayMaster_Red;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Player;

//this class will store Random stuff like anti-ban and Idle timers
public class Random {
	
	static void waitUntilIdle(int min, int max) {// waits min max time + until you'r not doing anything
		General.sleep(min, max);
		long t = System.currentTimeMillis();
		while (Timing.timeFromMark(t) < General.random(min, max)) {

			if (Player.isMoving() || Player.getAnimation() != -1) {
				t = System.currentTimeMillis();
			} else {
				break;
			}
			General.sleep(50, 150);
		}
	}

}
