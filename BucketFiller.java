package scripts.clayMaster_Red;

import org.tribot.api.General;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Game;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;

import scripts.redSpark.antiBan.AntiBan;
import scripts.redSpark.api.walking07.Walking07;
 
//import scripts.redSpark.api2007.Mouse07;

public class BucketFiller extends Node {
		private Variables variables;
		public BucketFiller(Variables variables){
			this.variables = variables;
		}
       
        public void execute() {
                //If we are outside the bank it will try to fill buckets from the well
                //Not using inWellArea to speed up the clicking
                if (Locations.closeToWell(10)) {
                        fillBuckets();
                //Walks to well if we are not in Well Area
                } else {
                	
                	//TODO
                        walkToWell();
    	
                }
        }
 
        public boolean validate() {
                //Checks if we are on Stage 0(Bucket filling) and if we are stocked up
                if (variables.getStage() == 0 && !variables.needRestock()&& variables.fillBuckets()) {
                		if(Variables.DEBUG)
                			General.println("BucketFiller VALIDATE = TRUE"); //Debug line
                        return true;
                }
                return false;
        }
 
        private boolean walkToWell() {// Walks form bank to well
                //Gets a random tile in Well Area to walk to
                RSTile randomWellTile;
                randomWellTile = Variables.getWaterSourceArea().getRandomTile();
             //   Mouse07.fixSelected();
                //Walks to well tile if we have on and if we are not moving
                if(randomWellTile != null && !Player.isMoving()){   
                		RSTile[] path = Walking.generateStraightPath(randomWellTile);
                		if(path.length > 0){
                			return Walking07.walkPath(path, 10);
                		}
                	
                	
                	
                	
                       // Walking.walkTo(randomWellTile);
                      //  AntiBan.activateRun();
                      //  General.sleep(500, 1500);
                      //  return true;
                }
                return false;
        }
        //Used to check if we are doing work(Filling buckets)
        int lastWaterBucketCount = Inventory.getCount(Variables.getBucketId("Water"));
        boolean working = false;//keeps track of work status
        long t = System.currentTimeMillis();//time variable used for checking if work is being done
       
        //Fills empty buckets with water from the well
        private boolean fillBuckets() {
                AntiBan.pickUpMouse();//will try to pick up mouse is its out of the game window
                if(working){//AntiBan
                        AntiBan.examineObject();
                        AntiBan.moveCamera();
                        AntiBan.leaveGame();
                        AntiBan.mouseMovement();
                        AntiBan.rightClick();
                }
                RSObject[] well = Objects.findNearest(20, Variables.getWaterSource());//Stores well
                RSItem[] emptyBucket = Inventory.find(Variables.getBucketId("Empty"));//Finds empty buckets in the inventory
                String upText = Game.getUptext();//Stores up text
               
 
                // Checks if there is a Well near by
                if (well != null && well.length > 0) {
                        //General.println("Well found"); //Debug line
                        RSTile wellTile = well[0].getPosition();//Gets well position
                        //If well is not on screen, turn the camera towards is
                        if (!wellTile.isOnScreen()){
                                Camera.turnToTile(wellTile);
                        }
                        //Check of we have empty buckets
                        if(emptyBucket != null && emptyBucket.length > 0){
                                //Click on empty bucket
                                if (!working && !upText.equalsIgnoreCase("Use Bucket ->")) {
                                        if(!emptyBucket[0].click("Use"))
                                        	return false;
                                        
                                        AntiBan.waitItemInteractionDelay();
                                }
                                //Click on the well if bucket is highlighted
                                if (upText != null && upText.equalsIgnoreCase("Use Bucket ->")) {
                                                if(well[0].click("Use Bucket -> Well")){
                                                       // Mouse07.fixSelected();
                                                        AntiBan.waitItemInteractionDelay();
                                                        working = true;//We started filling buckets, so we are working
                                                        //General.println("Clicking on well");//Debug line
                                                        AntiBan.waitUntilIdle(500, 1000);
                                                        return true;
                                                }
                                                
                                }
                                //used to check work status every 2 secs
                                if(System.currentTimeMillis() - t > 2000){
                                        //General.println("BF:"+System.currentTimeMillis() - t);//Debug line
                                        t = System.currentTimeMillis();//resets t every 2 secs
                                        //if in 2 sec no works was done we change out work status to false
                                        if(Inventory.getCount(Variables.getBucketId("Water")) == lastWaterBucketCount){
                                                //General.println("Not Working");//Debug line
                                                working= false;
                                        }
                                        if(Inventory.getCount(Variables.getBucketId("Empty"))==0){
                                        		//no more empty buckets, get more
                                        		working= false;
                                        }
                                        lastWaterBucketCount = Inventory.getCount(Variables.getBucketId("Water"));
                                }
                                //here goes the anti-bank since we are waiting for buckets to fill
                               
                                General.sleep(100, 500);
                        //all buckets full let's go re-stock
                        }else{
                                //General.println("No empty buckets");//Debug line
                                working = false;
                                variables.setRestock(true);
                        }
                        //can't find well
                }else{
                        //General.println("Can't find well");//Debug line
                        working = false;
                }
                return false;
 
        }
 
}