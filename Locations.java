package scripts.clayMaster_Red;


import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;
 
/*Class for storing locations
 * Used for checking is the player is in an area or close to an object
 */
 
public class Locations {
       
        static boolean inBankArea(){//Check if your in Bank Area
                if((Variables.getBankArea()[0].contains(Player.getPosition()))){
                        return true;
                }
                if((Variables.getBankArea()[1].contains(Player.getPosition()))){
                		return true;
                }
               			
                return false;  
        }
        static boolean closeToBank(int distance){
        	 RSObject[] bank = Objects.findNearest(20, Variables.getBankId());
             if(bank.length >0 && bank[0] != null){
             		if(Player.getPosition().distanceTo(bank[0].getPosition()) < distance){
             			return true;
             		}
             }
             return false;
        }
       
        static boolean closeToWell(int distance){ //Checks is there is a well near by
                RSObject[] well = Objects.findNearest(20, Variables.getWaterSource());
                if(well.length>0 && well[0] != null){
                		if(Player.getPosition().distanceTo(well[0].getPosition()) < distance)
                			return true;
                        //RSTile wellTile = well[0].getPosition();                       
                        /**,
                        if (wellTile.isOnScreen()){//Checks is well is on screen
                                return true;
                        }else{
                                General.println("Rotating camer for well");
                                Camera.turnToTile(wellTile);//Turns the camera towards the well
                        }
                        */
                }
                return false;
        }
        static String getLocation(RSTile playerPosition){
        	
        	
        	return "";
        }
}
