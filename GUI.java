package scripts.clayMaster_Red;
 
import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.JCheckBox;
import javax.swing.JButton;
 
 
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
 
public class GUI {
		private Variables variables;
        private JFrame frame;
        JCheckBox chckbxUseGe;
        /**
         * Create the application.
         */
        public GUI(Variables variables) {
        		this.variables = variables;
        		initialize();
        }
 
		/**
         * Launch the application.
        
        public static void main(String[] args) {
                EventQueue.invokeLater(new Runnable() {
                        public void run() {
                                try {
                                        GUI window = new GUI(variables);
                                        window.frame.setVisible(true);
                                } catch (Exception e) {
                                        e.printStackTrace();
                                }
                        }
                });
        }
 

        /**
         * Initialize the contents of the frame.
         */
        private void initialize() {
                frame = new JFrame();
                frame.setBounds(100, 100, 360, 420);
                frame.setResizable(false);
                frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                frame.getContentPane().setLayout(null);
               
                JLabel txtRedClayMaster = new JLabel("Red ClayMaster");
                txtRedClayMaster.setFont(new Font("Arial", Font.BOLD, 30));
                txtRedClayMaster.setHorizontalAlignment(SwingConstants.CENTER);
                txtRedClayMaster.setBounds(-2, 0, 359, 50);
                frame.getContentPane().add(txtRedClayMaster);
               
                JLabel txtMouseSpeed = new JLabel("Mouse Speed");
                txtMouseSpeed.setHorizontalAlignment(SwingConstants.CENTER);
                txtMouseSpeed.setFont(new Font("Arial", Font.PLAIN, 20));
                txtMouseSpeed.setBounds(-2, 70, 359, 20);
                frame.getContentPane().add(txtMouseSpeed);
               
                JSlider sliderMouseSpeed = new JSlider();
                sliderMouseSpeed.setPaintLabels(true);
                sliderMouseSpeed.setValue(100);
                sliderMouseSpeed.setPaintTicks(true);
                sliderMouseSpeed.setMajorTickSpacing(50);
                sliderMouseSpeed.setMaximum(300);
                sliderMouseSpeed.setMinimum(50);
                sliderMouseSpeed.setMinorTickSpacing(10);
                sliderMouseSpeed.setBounds(12, 90, 330, 60);
                frame.getContentPane().add(sliderMouseSpeed);
                
                JComboBox<String> locations = new JComboBox<String>();
                locations.setModel(new DefaultComboBoxModel<String>(new String[] {"Edgeville", "More Comming soon"}));
                locations.setSelectedIndex(0);
                locations.setBounds(12, 163, 330, 22);
                frame.getContentPane().add(locations);
               
                JCheckBox chckbxFillBuckets = new JCheckBox("Fill Buckets");
                chckbxFillBuckets.setSelected(true);
                chckbxFillBuckets.setBounds(40, 216, 113, 25);
                frame.getContentPane().add(chckbxFillBuckets);
              
                JCheckBox chckbxMakeSoftClay = new JCheckBox("Make Soft Clay"); 
                chckbxMakeSoftClay.addActionListener(new ActionListener() {
                	public void actionPerformed(ActionEvent arg0) {
                		chckbxUseGe.setEnabled(false);
                	}
                });
                           
                chckbxMakeSoftClay.setSelected(true);
                chckbxMakeSoftClay.setBounds(40, 246, 113, 25);
                frame.getContentPane().add(chckbxMakeSoftClay);
                
                chckbxUseGe = new JCheckBox("Use GE(BETA)");              
                chckbxUseGe.setBounds(40, 277, 113, 25);
                chckbxUseGe.setEnabled(false);
                frame.getContentPane().add(chckbxUseGe);
                
                JCheckBox chckbxTidyBank = new JCheckBox("Tidy Bank");
                chckbxTidyBank.setSelected(true);
                chckbxTidyBank.setToolTipText("Keeps atleast one of each item");
                chckbxTidyBank.setBounds(197, 246, 113, 25);
                frame.getContentPane().add(chckbxTidyBank);
               
                JCheckBox chckbxAdvancedPaint = new JCheckBox("Advanced Paint");
                chckbxAdvancedPaint.setToolTipText("Wil be added soon");
                chckbxAdvancedPaint.setEnabled(false);
                chckbxAdvancedPaint.setBounds(197, 276, 145, 25);
                frame.getContentPane().add(chckbxAdvancedPaint);
                
                JButton btnStart = new JButton("Start");
                btnStart.setBounds(70, 347, 97, 25);
                frame.getContentPane().add(btnStart);
                btnStart.addActionListener(new ActionListener() {
                	public void actionPerformed(ActionEvent e) {
                            variables.setMouseSpeed(sliderMouseSpeed.getValue());
                            variables.location(locations.getSelectedIndex());
                            variables.setFillBuckets(chckbxFillBuckets.isSelected());
                            variables.setMakeSoftClay(chckbxMakeSoftClay.isSelected());
                            variables.setTidyBank(chckbxTidyBank.isSelected());
                            variables.setUseGE(chckbxUseGe.isSelected());
                            variables.setGUIState(false);
                            variables.setRun(true);
                           
                            if(!chckbxFillBuckets.isSelected()){
                                    variables.setStage(1);
                            }else{
                                    variables.setStage(0);
                            }
                            frame.setVisible(false);
                           
                    }
                });
                
               
                JButton btnCancel = new JButton("Cancel");
                btnCancel.setBounds(190, 347, 97, 25);
                frame.getContentPane().add(btnCancel);
                btnCancel.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                            variables.setGUIState(false);
                            variables.setRun(false);
                            frame.setVisible(false);
                    }
                });           
        }
 
        public void setVisible(boolean b) {
                frame.setVisible(b);
               
        }
}