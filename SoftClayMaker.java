package scripts.clayMaster_Red;

import org.tribot.api.General;
import org.tribot.api2007.Game;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSItem;
import scripts.redSpark.antiBan.AntiBan;
 
//import scripts.redSpark.api2007.Mouse07;
 
public class SoftClayMaker extends Node{
		private Variables variables;
		public SoftClayMaker(Variables variables){
			this.variables = variables;
		}
		
 
        @Override
        public void execute() {
                makeSoftClay();
               
        }
 
        @Override
        public boolean validate() {
                //Checks if we are on Stage 1(Making soft clay) and if we are stocked up
        	
                if(variables.getStage() == 1 && !variables.needRestock() && variables.makeSoftClay()){
                	if(Variables.DEBUG)
                        	General.println("SoftClayMaker VALIDATE = TRUE");//Debug line
                        return true;
                }
                
                return false;
        }
        //Used to check if we are working
        int lastWaterBucketCount = Inventory.getCount(Variables.getBucketId("Water"));
        static boolean working = false;//our working status
        long t = System.currentTimeMillis();//Time variable used in checking if we are working
        //Making soft clay method
        private boolean makeSoftClay(){
                //AntiBan
                AntiBan.pickUpMouse();//will try to pick up mouse is its out of the game window
                if(working){//AntiBan
                        AntiBan.examineObject();
                        AntiBan.moveCamera();
                        AntiBan.leaveGame();
                        AntiBan.mouseMovement();
                        AntiBan.rightClick();
                }
                RSItem[] waterBucket = Inventory.find(Variables.getBucketId("Water"));//Finds water buckets in inventory
                RSItem[] clay = Inventory.find(Variables.getClayId());//Find clay in inventory
                String upText = Game.getUptext();//stores up text
                //If we are not working and we don't have the correct up text we will click on water bucket
                if (!working && !upText.equalsIgnoreCase("Use Bucket of water ->")) {
                		if(waterBucket.length != 0){
                			if(!waterBucket[waterBucket.length -1].click("Use"))
                					return false;//failed to click
                			AntiBan.waitItemInteractionDelay();
                		}
                }
                //Checks if we have water buckets
                if(waterBucket != null && waterBucket.length > 0){
                        //Checks if we have clay
                        if(clay != null && clay.length > 0 ){
                                variables.setRestock(false);
                                //Checks if we have a water bucket highlighted
                                if (upText != null && upText.equalsIgnoreCase("Use Bucket of water ->")) {
                                        if(!clay[0].click("Clay"))
                                        	return false;//failed to click
                                        working = true;//We started working(Making soft clay)
                                        //General.println("Clicking water bucket -> clay");//Debug line
                                        //Mouse07.fixSelected();
                                        AntiBan.waitItemInteractionDelay();
                                        return true;
                                }
                                //used to check work status every 2 secs       
                                if(System.currentTimeMillis() - t  > 2000){
                                        //General.println("SCM:"+System.currentTimeMillis() - t);//Debug line
                                        t = System.currentTimeMillis();//resets t every 2 secs
                                        //if in 2 sec no works was done we change out work status to false
                                        if(Inventory.getCount(Variables.getBucketId("Water")) == lastWaterBucketCount){
                                                //General.println("Not Working");//Debug line
                                                working= false;
                                        }
                                        if(Inventory.getCount(Variables.getClayId()) == 0 || Inventory.getCount(Variables.getBucketId("Water")) == 0 ){
                                        	//no more clay/water buckets, so get some more
                                        		working= false;
                                        }
                                        lastWaterBucketCount = Inventory.getCount(Variables.getBucketId("Water"));     
                                }
                                //Using make soft clay interface
                                RSInterfaceChild clayInterface = Interfaces.get(309, 2);
                                if(clayInterface != null && !clayInterface.isHidden()){
                                        clayInterface.click("All");
                                       // Mouse07.fixSelected();
                                        AntiBan.waitItemInteractionDelay();
                                        General.sleep(1500, 2000);
                               
                                }
                       
                        }else{
                                //Out of clay need to re-stock
                                General.println("Out of clay");//Debug line
                                variables.setRestock(true);
                        }
                }else{                    
                        variables.setRestock(true);
                }
                return false;
        }
       
}
