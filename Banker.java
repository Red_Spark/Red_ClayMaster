package scripts.clayMaster_Red;


import org.tribot.api.General;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Walking;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSObject;


import org.tribot.api2007.Player;

import org.tribot.api2007.Camera;

import org.tribot.api2007.types.RSTile;

import scripts.redSpark.antiBan.AntiBan;
import scripts.redSpark.api.banking07.Banking07;
import scripts.redSpark.api.inventory07.Inventory07;
import scripts.redSpark.api.walking07.Walking07;
 
//import scripts.redSpark.api2007.Mouse07;
 
public class Banker extends Node {
		private Variables variables;

		public Banker(Variables variables){
			this.variables = variables;
		}
 
        public void execute() {
               
                if (Locations.closeToBank(10)) {//If in Bank Area will try to bank
                        bank();
                } else {
                	
                	//TODO
                        if(walkToBank())//Walks to bank
                        	bank();
                }
 
        }
 
        public boolean validate() {
                if (variables.needRestock()) {// Checks if we need to bank
                	if(Variables.DEBUG)
                        General.println("Bank VALIDATE = TRUE"); //Debug line
                    return true;
                }
                return false;
        }
 
        private boolean bank() {
                if (!Banking.isBankScreenOpen()) {
                        // The bank screen is not open. Let's open it.
                        RSObject[] bank = Objects.findNearest(20, Variables.getBankId());//Finds nearest bank booth
                        if(bank !=null && bank.length > 0){
                                RSTile bankTile = bank[0].getPosition();//Gets position of the bank booth
                                //Turns the camera towards the bank booth
                                if (!bankTile.isOnScreen()){
                                        Camera.turnToTile(bankTile);
                                        if (!bankTile.isOnScreen()){
                                                int cameraAngle = Camera.getCameraAngle();
                                                Camera.setCameraAngle(cameraAngle-10);
                                        }
                                }else{
                                        //Click on the bank booth and waits until Player is Idle
                                        if(!Banking.openBank())//Using this method for anti-ban compliance
                                        	return false;//failed to open bank
                                        AntiBan.waitUntilIdle(500, 1000);
                                        return true;
                                }
                        }
                        return false;
                }
                RSItem[] inventory  = Inventory.getAll();             
                int count = Inventory.getCount(variables.getSoftClayId());
                if (!(variables.needRestock() && inventory.length > 1 && Banking.depositAll() < 1)) {
                        variables.addClay(count);//only adds the clay if we successfully deposit it
                        if(Variables.DEBUG)
                        	General.println("Soft Clay Deposit:"+count);
                        if(!Inventory07.waitForDepositAll(General.random(400, 7000)))
                        	return false;//items failed to deposit, return false
                }else{
                        // We failed to click the deposit all button. Let's exit and
                        // return false.
                        return false;
                }
                //Stage 0 will bank for water filling
                //Stage 1 will bank for making soft clay
                if(Variables.DEBUG)
                	General.println("Stage:"+variables.getStage());//debugging line
                
                switch(variables.getStage()){
                        case 0:
                                if (!withdrawBuckets())
                                        return false;
                                break;
                        case 1:
                                if (!withdrawClay())
                                        return false;
                                break;
                }
                ///Closes the bank window
                if (!Banking.close()) {
                        return false;
                }
                General.sleep(200, 500);
                return true;
 
        }
 
        // METHODS
       
        //Used for Stage 0(Re-stocking with empty buckets)
       
        private boolean withdrawBuckets() {// Withdraws empty buckets
                RSItem[] emptyBuckets = Banking.find(Variables.getBucketId("Empty"));//Finds empty buckets in bank
                int emptyBucketCount;
                if(emptyBuckets.length > 0){
                	emptyBucketCount = emptyBuckets[0].getStack();
                	 if (emptyBucketCount > variables.tidyBank(variables.tidyBank())) {                		
                         //Withdraws empty buckets using "Withdraw-All" option
                         if(variables.tidyBank() && emptyBucketCount <=28){
                         		Banking07.withdrawItem(emptyBuckets[0], -1, General.random(4000, 7000));//Custom banking method.1st var = RSItem, 2nd var = ammont(0 = all), 3rd var = max wait time
                                 //emptyBuckets[0].click("Withdraw-All-but-one");
                                 //Inventory07.waitForItem(Variables.getBucketId("Empty"), General.random(1500, 2000));
                                 variables.setRestock(false);//Sets needRestock to false, because we just re-stocked
                                 AntiBan.waitItemInteractionDelay();
                         }else if (Banking07.withdrawItem(emptyBuckets[0], 0, General.random(4000, 7000))) {
                         		if(Variables.DEBUG)
                         			General.println("Withdrawing Empty Buckets"); //Debug line
                                 variables.setRestock(false);//Sets needRestock to false, because we just re-stocked
                                 AntiBan.waitItemInteractionDelay();
                                 return true;
                         }
  
                	 }else{
                         //Since we are out of empty buckets, we will start making soft clay
                         if(variables.makeSoftClay()){
                                 variables.setStage(1);//Stage 1 is making soft clay
                         }else{
                                 variables.setRun(false);
                         }
                         General.println("No more buckets"); //Debug line
                	}
                }
               
                return false;
        }
       
        //Used for Stage 1(Re-stocking with water buckets + clay to make soft clay)
        private boolean withdrawClay(){
               
                RSItem[] waterBuckets = Banking.find(Variables.getBucketId("Water"));//Find water buckets in bank
                RSItem[] clay = Banking.find(Variables.getClayId());//Finds clay in banks
                if(waterBuckets.length > 0 && clay.length > 0){
                        int waterBucketCount = waterBuckets[0].getStack();
                        int clayCount = clay[0].getStack();
                        if(waterBucketCount > variables.tidyBank(variables.tidyBank())
                                        && clayCount > variables.tidyBank(variables.tidyBank())){                                              
                                                //Withdraws 14 of water buckets and clay
                                                if(variables.tidyBank() && (waterBucketCount <= 14 || clayCount <= 14)){
                                                        if(waterBucketCount <=14 && Banking07.withdrawItem(waterBuckets[0], -1, General.random(2000, 3000)) && Banking07.withdrawItem(clay[0], -1, General.random(4000, 7000)) ){   
                                                                General.println("Withdrawing water buckets(Last of them)"); //Debug line
                                                        }
                                                        if(clayCount <=14 && Banking07.withdrawItem(clay[0], -1, General.random(4000, 7000)) && Banking07.withdrawItem(waterBuckets[0], -1, General.random(4000, 7000))){  
                                                                General.println("Withdrawing clay(Last of them)"); //Debug line
                                                        }
                                                        AntiBan.waitItemInteractionDelay();
                                                        variables.setRestock(false);//Sets needRestock to false, because we just re-stocked
                                                        SoftClayMaker.working = false;//sets working to false to speed up clicking Water bucket ->Clay
                                                        return true;
                                                }else if(Banking07.withdrawItem(waterBuckets[0], 14, General.random(4000, 7000)) && Banking07.withdrawItem(clay[0], 14, General.random(4000, 7000))){
                                                		if(Variables.DEBUG)
                                                			General.println("Withdrawing water buckets + clay"); //Debug line
                                                        AntiBan.waitItemInteractionDelay();
                                                        variables.setRestock(false);//Sets needRestock to false, because we just re-stocked
                                                        SoftClayMaker.working = false;//sets working to false to speed up clicking Water bucket ->Clay
                                                        return true;
                                                }
                                        //Out of water buckets in the bank, need to fill them up
                                        }else if(waterBucketCount == variables.tidyBank(variables.tidyBank())){
                                                if(!variables.fillBuckets()){
                                                        variables.setRun(false);//stops the script
                                                }
                                                variables.setStage(0);//Stage 0 is filling buckets
                                                General.println("No more water buckets"); //Debug line
                                               
                                        //Out of clay in the bank, need to stop the script
                                        }else if(clayCount == variables.tidyBank(variables.tidyBank())){
                                                General.println("No more clay"); //Debug line
                                                if(variables.useGE()){
                                                	variables.setStage(2);//buying supply stage
                                                }else{
                                                	variables.setRun(false);//stops the script
                                                }
                                        }
                                                
                }      
               
               
                return false;
        }
 
        private boolean walkToBank() {// Walks form well to bank.
                //Gets a random tile in Bank Area to walk to
                //RSTile randomBankTile;
                //randomBankTile = Variables.getBankArea().getRandomTile();
                //Mouse07.fixSelected();
        		RSObject[] banker = Objects.findNearest(20, Variables.getBankId());
                if(banker.length > 0 && banker[0] != null && !Player.isMoving()){
                		RSTile[] path = Walking.generateStraightPath(banker[0].getPosition());
                		if(path.length > 0){
                			return Walking07.walkPath(path, 10);
                		}
                        //Walking.walkTo(randomBankTile);
                        //AntiBan.activateRun();
                        //General.sleep(1000,1500);
                        //return true;
                }
                return false;
        }
}